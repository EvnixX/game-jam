using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CocA : MonoBehaviour
{
    [SerializeField] private int score_C;
    [SerializeField] private int money;

    public Toggle toggle;
    public TextMeshProUGUI coca;
    public TextMeshProUGUI money_T;
    // Start is called before the first frame update
    private void Start()
    {
    }

    private void Update()
    {
        toggle = FindObjectOfType<Toggle>();
        if (money >= 100)
        {
         toggle.isOn = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Coca")
        {
            score_C++;
            coca.text = "" + score_C;
        }


        if (other.gameObject.tag == "Money")
        {
            while (score_C > 0)
            {
                score_C--;
                coca.text = "" + score_C;

                if (score_C <= 100)
                {
                    money += 10;
                    money_T.text = "" + money;
                }
                if (score_C == 0)
                {
                    break;
                }
            }
        }

        while (other.gameObject.tag == "Deudas" && money >= 100)
        {
            money -= 100;
            money_T.text = "" + money;
            if (money == 0)
            {
                break;
            }
        }

    }
}
