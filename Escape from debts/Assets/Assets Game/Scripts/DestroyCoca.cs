using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCoca : MonoBehaviour
{
    public GameObject coca;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(coca);
        }
    }
}
