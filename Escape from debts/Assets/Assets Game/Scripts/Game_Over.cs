using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game_Over : MonoBehaviour
{
    public GameObject bar;
    public int timer;
    public string scene;
    // Start is called before the first frame update
    void Start()
    {
        AnimatedBar();
    }

    public void AnimatedBar()
    {
        LeanTween.scaleX(bar, 1, timer).setOnComplete(ChangeScene);
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(scene);
    }

} 
