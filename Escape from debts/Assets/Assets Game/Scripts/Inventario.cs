 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Inventario : MonoBehaviour
{
    public GameObject inventory;
    private int allSlots;

    public List<GameObject> slots = new List<GameObject>();
    public GameObject slotsHolder;

    private void Start()
    {
        allSlots = slotsHolder.transform.childCount;

        for (int i = 0; i < allSlots; i++)
        {
            slots[i] = slotsHolder.transform.GetChild(i).gameObject;
            if (slots[i].GetComponent<Slots>().objeto == null)
            {
                slots[i].GetComponent<Slots>().empty = true; 
            }
        }

        inventory.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Items")
        {
            GameObject itemPicketUp = other.gameObject;

            Items item = itemPicketUp.GetComponent<Items>();

            AddItem(itemPicketUp,item.ID,item.type,item.description,item.icon);
        }

        if (other.tag == "Caja")
        {
            /*
            foreach (var item in slots)
            {
                if (item.GetComponent<Slots>().empty == true)
                {
                    slots.Remove(item);
                }
            }*/
          try
          {

            for (int i = 0; i < allSlots; i++)
            {

              foreach (var item in slots)
              {
                if (item.GetComponent<Slots>().empty == false)
                {
                    item.transform.GetChild(0).GetComponentInChildren<Image>().enabled = false;
                    slots.Remove(slots[i]);
                    print("Funciona");
                }
              }
            }
          }
            catch(Exception e)
            {

            }

            finally
            {
              
            }
        }
    }

    public void AddItem(GameObject item,int itemID, string itemType,string itemDescription, Image itemIcon)
    {


        for (int i = 0; i < allSlots; i++)
        {

             if (slots[i].GetComponent<Slots>().empty)
             {
                item.GetComponent<Items>().pickedUp = true;
                slots[i].GetComponent<Slots>().objeto = item;
                slots[i].GetComponent<Slots>().ID = itemID;
                slots[i].GetComponent<Slots>().type = itemType;
                slots[i].GetComponent<Slots>().description = itemDescription;
                slots[i].GetComponent<Slots>().icon = itemIcon;

                item.transform.parent = slots[i].transform;
                item.SetActive(false);

                slots[i].GetComponent<Slots>().UpdateSlots();

                slots[i].GetComponent<Slots>().empty = false;
    
               return;
             }
        }
    }
}
