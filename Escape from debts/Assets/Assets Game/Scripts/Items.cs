using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Items : MonoBehaviour
{
    public int ID;
    public string type;
    public string description;
    public Image icon;
    
    [HideInInspector]
    public bool pickedUp;
    
}
