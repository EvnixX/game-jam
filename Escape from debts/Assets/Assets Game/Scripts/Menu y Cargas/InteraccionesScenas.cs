using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteraccionesScenas : MonoBehaviour
{
    private bool cavamActivado;
    public GameObject panelDCarga;
    public GameObject objetoDInteractuar;
    public string proximaScena;

    bool activo;


    private void Update()
    {

        if (Input.GetKeyDown("e") && activo)
        {
            cavamActivado = !cavamActivado;
            SceneManager.LoadScene(proximaScena);

            print("Funciona");
        }
        if (cavamActivado)
        {
            panelDCarga.SetActive(true);
        }
        else
        {
            panelDCarga.SetActive(false);
        }

        if (Input.GetKeyUp("e"))
        {
            Invoke("Cargar", 10f);
        }
    }

    private void Cargar()
    {
        panelDCarga.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objetoDInteractuar.SetActive(true);
            activo = true;
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objetoDInteractuar.SetActive(false);
            activo = false;
        }
    }
}
