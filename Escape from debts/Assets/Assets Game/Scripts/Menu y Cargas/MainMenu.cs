using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void EscenaJuego()
    {
        SceneManager.LoadScene("Casa");
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void Instrucciones()
    {
        SceneManager.LoadScene("Como Jugar");
    }
    public void Salir()
    {
        Application.Quit();
    }



}
