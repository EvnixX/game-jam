using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class int_Baño : MonoBehaviour
{
    public GameObject panelCarga;
    public GameObject objetoInteractuar;

    bool active;

    private void Update()
    {
        if (Input.GetKeyDown("e") && active)
        {
            panelCarga.SetActive(true);
            print("Activo");
        }
        if (Input.GetKeyUp("e"))
        {
            Invoke("Cargar", 10f);

        }
    }

    private void Cargar()
    {
        panelCarga.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objetoInteractuar.SetActive(true);
            active = true;
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            objetoInteractuar.SetActive(false);
            active = false;
        }
    }
}
