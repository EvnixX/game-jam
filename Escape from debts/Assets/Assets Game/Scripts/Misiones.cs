using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Misiones : MonoBehaviour
{
    private bool cavasActivado;
    public GameObject canvasMisiones;
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            cavasActivado = !cavasActivado;
        }
        if(cavasActivado)
        {
            canvasMisiones.SetActive(true);
        }
        else
        {
            canvasMisiones.SetActive(false);
        }
    }
}
                                                                            