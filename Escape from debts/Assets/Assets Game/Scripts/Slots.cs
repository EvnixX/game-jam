using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slots : MonoBehaviour
{
    public GameObject objeto;
    public int ID;
    public string type, description;    
    public Image icon;
    public bool empty;

    public Transform slotIconObject;

    private void Start()
    {
        slotIconObject = transform.GetChild(0);
    }

    public void UpdateSlots()
    {
        slotIconObject.GetComponent<Image>().sprite = icon.sprite;
    }
}
