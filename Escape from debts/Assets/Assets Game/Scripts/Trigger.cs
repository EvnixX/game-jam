using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trigger : MonoBehaviour
{
    public Toggle toggle;
    private void Update()
    {
        toggle = FindObjectOfType<Toggle>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            toggle.isOn = true;
        }
    }
}
