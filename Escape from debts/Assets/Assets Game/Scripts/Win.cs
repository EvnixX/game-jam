using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{
    public string scene_Win;

    private void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene(scene_Win);
    }


}
